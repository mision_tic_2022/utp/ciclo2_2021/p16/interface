public class Alumno implements Persona {
    /**************
     * Atributos
     **************/
    private String nombre;
    private String apellido;
    private int edad;
    private String cedula;
    private char genero;

    public Alumno(String nombre, String apellido, int edad, String cedula, char genero){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.cedula = cedula;
        this.genero = genero;
    }

    public void asignar_salon(){
        System.out.println("Salón asignado");
    }
}
